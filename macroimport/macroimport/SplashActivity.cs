﻿
using System;

namespace macroimport
{
    using System.Threading;
    using Android.App;
    using Android.Content.PM;
    using Android.OS;
    using Android.Widget;

    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            try
            {

            base.OnCreate(bundle);
            Thread.Sleep(100);
            StartActivity(typeof(MainActivity));

            }
            catch (Exception)
            {
                Toast.MakeText(this, "conexión de internet fallida", ToastLength.Long).Show();
            }
        }
    }
}