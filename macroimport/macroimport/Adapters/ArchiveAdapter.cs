﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Provider;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using MacroImport;

namespace macroimport
{
    public class ArchiveAdapter : BaseAdapter<Archive>
    {
        List<Archive> items;
        Activity context;

        public ArchiveAdapter(Activity context, List<Archive> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Archive this[int position]
        {
            get
            {
                return items[position];
            }
        }

        public override int Count
        {
            get
            {
                return items.Count;
            }
        }

        


        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            try
            { 

                var item = items[position];

                if (convertView == null)
                {
                    convertView = context.LayoutInflater.Inflate(Resource.Layout.MainList, null);
                }

                ImageView imageView = (ImageView)convertView.FindViewById(Resource.Id.Image);
                
                TextView text2 = (TextView)convertView.FindViewById(Resource.Id.Text2);

                TextView textView = (TextView)convertView.FindViewById(Resource.Id.tagText);

                text2.Text = item.Date;

                if (item.File == "docx")
                {
                    imageView.SetImageResource(Resource.Drawable.docx);
                   
                }
                else
                {
                    if (item.File == "jpg")
                    {
                        imageView.SetImageResource(Resource.Drawable.jpg);
                        
                    }
                    else
                    {
                        if (item.File == "pdf")
                        {
                            imageView.SetImageResource(Resource.Drawable.pdf);
                            
                        }
                        else
                        {
                            imageView.SetImageResource(Resource.Drawable.xlsx);
                            
                        }
                    }
                    
                }

                if (item.Relevant == 1 )
                {
                    
                    textView.SetBackgroundResource(Resource.Drawable.tagreen);
                }
                else
                {
                    if (item.Relevant == 2)
                    {
                        
                        textView.Text = "Actualizar";
                        textView.SetBackgroundResource(Resource.Drawable.tagred);
                    }
                }
                
                return convertView;
            }
            catch (Exception)
            {               
                return convertView;
            }
        }
    }
}