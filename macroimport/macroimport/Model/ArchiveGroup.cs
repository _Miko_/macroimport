﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace macroimport
{
    public class ArchiveGroup
    {
        public int ArchivesGroupsId
        {
            get;
            set;
        }

        public List<Archive> Archives
        {
            get;
            set;
        }
    }
}
