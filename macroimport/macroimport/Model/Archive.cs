﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace macroimport
{
    public class Archive
    {

        public int FileId
        {
            get;
            set;
        }

        public string Date
        {
            get;
            set;
        }

        public string File
        {
            get;
            set;
        }

        public int Relevant
        {
            get;
            set;
        }
       
        public string Url
        {
            get;
            set;
        }
    }
}
