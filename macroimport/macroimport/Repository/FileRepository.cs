﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace macroimport
{
    public class FileRepository
    {
        string url =
             @"https://storage.googleapis.com/macroimport/archivos.json";

        public static List<ArchiveGroup> _archivegroups = new List<ArchiveGroup>();

        public FileRepository()
        {
            Task.Run(() => this.LoadDataAsync(url)).Wait();
        }
        private async Task LoadDataAsync(string uri)
        {

            if (_archivegroups != null)
            {
                string responsejsonstring = null;

                using (var httpClient = new HttpClient())
                {
                    try
                    {

                        Task<HttpResponseMessage> getresponse = httpClient.GetAsync(uri);
                        HttpResponseMessage response = await getresponse;
                        responsejsonstring = await response.Content.ReadAsStringAsync();

                        _archivegroups = JsonConvert.DeserializeObject<List<ArchiveGroup>>(responsejsonstring);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
        }

        public List<ArchiveGroup> GetGroupedFile()
        {
            return _archivegroups;
        }

        public List<Archive> GetFileForGroup(int fileGroupId)
        {
            var group = _archivegroups.Where(h => h.ArchivesGroupsId == fileGroupId).FirstOrDefault();

            if (group != null)
            {
                return group.Archives;
            }
            return null;
        }

        public List<Archive> GetAllFiles()
        {
            IEnumerable<Archive> archives =
                from archivegroup in _archivegroups
                from archive in archivegroup.Archives

                select archive;
            return archives.ToList<Archive>();
        }

        public Archive GetFileById(int fileId)
        {
            IEnumerable<Archive> archives =
                from archivegroup in _archivegroups
                from archive in archivegroup.Archives
                where archive.FileId == fileId
                select archive;

            return archives.FirstOrDefault();
        }
    }
}
