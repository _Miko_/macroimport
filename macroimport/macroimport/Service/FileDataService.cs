﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace macroimport
{
    public class FileDataService
    {
        public static FileRepository _filerepository = new FileRepository();

        public List<Archive> GetAllFiles()
        {
            return _filerepository.GetAllFiles();
        }

        public List<ArchiveGroup> GetGroupedFile()
        {
            return _filerepository.GetGroupedFile();
        }

        public List<Archive> GetFileForGroup(int fileGroupId)
        {
            return _filerepository.GetFileForGroup(fileGroupId);
        }

        public Archive GetFileById(int fileId)
        {
            return _filerepository.GetFileById(fileId);
        }
    }
}
